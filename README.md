# Clepsydra

<!--
## Name
Choose a self-explaining name for your project.
-->
### Project Status
As all the elements of the **NoxNivi** 'set', this is a Work In Progress,
therefore development pace is slow.

## Description
Clepsydra is a simple desktop widget that shows the UTC date and time, and local time on the lower right corner of the screen.

<!--
## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.
-->

## Screenshots
![Clepsydra running on NoxNivi](/screenshots/screen.png "Clepsydra")*Clepsydra*

## Installation
Clepsydra is a Python+QML simple widget.
Requirements:
- Python 3
- Pyside 6
- QML
- Monofur Nerd Font

As long as you have these in your system, just need to copy the /src folder and run `python __main__.py` within the /src folder.
Also can download the zipapp file and make it executable `chmod +x clepsydra` then run `./clepsydra`.
If you are running Arch linux, or a derived distro, you can add the NoxNivi repo and then run `pacman -S clepsydra` which will install it on /usr/bin. Then just run Clepsydra from a terminal, or as a start process from your Window Manager adding the 'clepsydra' command in the autostart preferences of your Window Manager.

## Usage
If you are using Arch, with Openbox Window Manager, and added the NoxNivi repository, once installed, you just need to add the line:
'''
clepsydra &
'''
to Openbox's autostart file and Clepsydra will start on user login.

<!--
## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.
-->

## Roadmap
TODO: 
- Allow changing the position on the desktop
- Add some menu for adjusting settings

<!--
## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
-->
