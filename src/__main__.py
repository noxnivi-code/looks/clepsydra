#!/usr/bin/env python

# Clepsydra
# (C) NoxNivi 2023
#
# Clepsydra is a desktop clock for the Noxnivi system that shows
# UTC date and time as well as local time, though focus is in UTC

# imports
import sys
import datetime
import subprocess
import importlib.resources

# from imports
from zoneinfo import ZoneInfo

from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtCore import Signal, QObject, QTimer

# Backend class that manages UTC and local time and date
class Backend(QObject):
    # calculates:
    #   UTC time
    #   UTC date
    #   Local time
    #   Local date (will be included in the local time)
    # and formats time as HH:MM
    # and formats date as YYYY.MM.DD
    # adds weekday as 3 letter

    # define the signal to pass arguments to the QML view
    updated = Signal(str,str, str, arguments=['utctime', 'utcdate', 'localtime'])

    def __init__(self):
        super().__init__()

        self.timer = QTimer()
        self.timer.setInterval(1000) # update every second
        self.timer.timeout.connect(self.updateTime)
        self.timer.start()


    def updateTime(self):
        # send the time and date to QML file
        # actual_utc_time = datetime.datetime.utcnow()
        actual_utc_time = datetime.datetime.now(tz=ZoneInfo("UTC"))
        # actual_local_time = datetime.datetime.now()
        get_local_tzone_cmd = "curl -sf https://ipapi.co/timezone"
        get_local_tzone = subprocess.getoutput(get_local_tzone_cmd)
        actual_local_time =datetime.datetime.now(tz=ZoneInfo(get_local_tzone))

        #all_datetime = [actual_utc_time, actual_local_time]
        #all_datetime = actual_utc_time.strftime("%a.%Y.%m.%d")
        utctime = actual_utc_time.strftime("%a>%H:%M")
        utcdate = actual_utc_time.strftime("%Y.%m.%d")
        localtime = actual_local_time.strftime("Local: %H:%M - %m.%d")
        #localdate = actual_local_time.strftime("%Y.%m.%d")

        #all_datetime = [utctime, utcdate, localtime, localdate]

        self.updated.emit(utctime, utcdate, localtime)


if __name__ == "__main__":
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()
    backend = Backend()

    #qmlFile = importlib.resources.files("gui").joinpath("main.qml") 
    qmlPack = importlib.resources.files("gui").joinpath("main.qml")
    qmlFile = importlib.resources.as_file(qmlPack)

    with qmlFile as qfile:
        engine.load(qfile)
    # engine.load("main.qml")

    backend = Backend()
    engine.rootObjects()[0].setProperty('backend', backend)
    backend.updateTime()

    if not engine.rootObjects():
        sys.exit(-1)

    sys.exit(app.exec())
