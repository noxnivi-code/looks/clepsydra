// Clepsydra
// Main.qml
// 202303
// 202307
// v 0.2.0
//
// QML file for the Clepsydra desktop widget
// Defines the window and qml widgets layout
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Window


Window {
  // Clepsydra main window holds the Layout
  id: root
  width: utcTimeLabel.implicitWidth
  height: columnContent.implicitHeight
  x: Screen.width - width - 30
  y: Screen.height - height - 30
  visible:  true
  color: "transparent"

  flags: Qt.FramelessWindowHint | Qt.WindowStaysOnBottomHint | Qt.NoDropShadowWindowHint | Qt.BypassWindowManagerHint

  // properties connected to backend
  property QtObject backend
  property string utcTime: ""
  property string utcDate: ""
  property string localTime: ""
  // TODO: might separate utcTime to add color?


  ColumnLayout {
    // column to arrange utc date, time and local time
    id: columnContent

    anchors.left: parent.left
    anchors.right: parent.right
    spacing: 0

    Text {
      // Utc day of week and time. Might change to a Row
      // to add color
      id: utcTimeLabel

      text: utcTime
      font.pixelSize: 48
      color: "#ffec9c"
      horizontalAlignment: Text.AlignRight
      Layout.fillWidth: true
    }

    Text {
      // Utc date in YYYY.MM.DD format
      id: utcDateLabel

      text: utcDate
      font.pixelSize:32
      color: "#ffffff"
      horizontalAlignment: Text.AlignRight
      Layout.fillWidth: true
    }

    Text {
      // Local time and date in MM.DD format
      id: localTimeLabel

      text: localTime
      font.pixelSize: 18
      color: "#bbbbbb"
      horizontalAlignment: Text.AlignRight
      Layout.fillWidth: true
    }


    Connections {
      // connection to the python main.py
      target: backend

      function onUpdated(utim, udat, lotim) {
        utcTime = utim;
        utcDate = udat;
        localTime = lotim;
      }
    }
  }

}
