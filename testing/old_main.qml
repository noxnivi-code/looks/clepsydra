// Clepsydra
// Main.qml
// 202303
// 202307
// v 0.2.0
//
// QML file for the Clepsydra desktop widget
// Defines the window and qml widgets layout

// imports
import QtQuick
import QtQuick.Window
import QtQuick.Controls
import QtQuick.Layouts

// clock window
Window {
  id: root
  // width: 300
  // height: 200
  implicitWidth: utcTimeText.implicitWidth
  //implicitHeight: timeContainer.implicitHeight
	visible: true
	x: Screen.width - width
	y: Screen.height - height
	color: "transparent"

	// flags: Qt.FramelessWindowHint | Qt.NoDropShadowWindowHint | Qt.WindowStaysOnBottomHint | Qt.BypassWindowManagerHint | Qt.WA_X11NetWmWindowTypeDesktop | Qt.Window
  // flags: Qt.WA_X11NetWmWindowTypeDesktop | Qt.WindowStaysOnBottomHint | Qt.FramelessWindowHint | Qt.NoDropShadowWindowHint

	// properties
	property QtObject backend
	property string utcTime: " "
	property string utcDate: " "
	property string localTime: " "

	Rectangle {
    id: timeContainer
    anchors {
      fill: parent
      margins: 20
    }
    //implicitWidth: utcTimeText.implicitWidth
    //implicitHeight: datetimeLayout.implicitHeight
		color: "transparent"

    ColumnLayout {
      id: datetimeLayout
      //anchors.right: parent.right
      //anchors.bottom: parent.bottom
      anchors.fill: parent
      //implicitWidth: utcTimeText.implicitWidth

			Text {
				// UTC time and weekday
        id: utcTimeText
        //anchors.right: parent.right
        Layout.fillWidth: true
        text: utcTime
        font.pixelSize: 48
        color: "#ffffff"
				horizontalAlignment: Text.AlignRight
			}

			Text {
				// UTC date yyyy.MM.dd
        id: utcDateText
        //anchors.right: parent.right
        Layout.fillWidth: true
        text: utcDate
        font.pixelSize: 32
        color: "#ffffff"
				horizontalAlignment: Text.AlignRight
			}

			Text {
				// local time and date MM.dd
        id: localTimeText
        //anchors.right: parent.right
        Layout.fillWidth: true
        text: localTime
        font.pixelSize: 18
        color: "#bbbbbb"
				horizontalAlignment: Text.AlignRight
			}
		}


		MouseArea {
			// set the whole column as the area to allow rightclick
			// and show the popup menu
			// FIXME: need to find out why this doesn´t work
			id: menuarea
			anchors.bottom: parent.bottom
			anchors.right: parent.right
			acceptedButtons: Qt.RightButton
			onClicked: (mouse) => {
        if (mouse.button === Qt.RightButton)
					contextMenu.popup()
			}
			Menu {
				id: contextMenu
				MenuItem {
					text: "Settings"
				}
				MenuSeparator {}
				MenuItem {
					text: "Quit"
					onTriggered: Qt.quit()
				}
			}
		}
	}

	Connections {
    // connection to the python file  main.py
		target: backend

		function onUpdated(utim, udat, lotim) {
			utcTime = utim;
			utcDate = udat;
			localTime = lotim;
		}
	}
}
