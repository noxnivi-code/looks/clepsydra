#!/usr/bin/env python

from zoneinfo import ZoneInfo
from datetime import datetime

dtutc = datetime.now(tz=ZoneInfo("Europe/Madrid"))
dtloc = datetime.now(tz=ZoneInfo("UTC"))

print(dtutc)
print(dtloc)
